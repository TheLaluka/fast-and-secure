all:
	sudo apt install golang
	go build -ldflags="-s -w" chall.go
	strip chall
	file chall

clean:
	rm chall
