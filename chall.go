package main

import (
	"fmt"
	"math/rand"
	"os"
	"os/exec"
	"strconv"
	"time"
)

func main() {
	s1 := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(s1)
	random := r1.Intn(9999999)
	outfile := "/tmp/chall/processes-" + strconv.Itoa(random)
	fmt.Println("Temporary output will be placed in " + outfile)
	exec.Command("/usr/bin/bash", "-c", "rm -rf /tmp/chall; mkdir -pv /tmp/chall").Run()
	exec.Command("/usr/bin/bash", "-c", "/usr/bin/ps -fau | /usr/bin/sort -uV -o "+outfile).Run()

	cmd := exec.Command("/usr/bin/cat", outfile)
	stdout, err := cmd.Output()

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Print(string(stdout))
	os.Exit(0)
}
