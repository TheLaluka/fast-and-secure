
FROM ubuntu:latest

RUN apt update && apt install openssh-server -y

RUN groupadd -g 1000 chall && \
    useradd -rm -d /home/chall -s /bin/bash -g chall -u 1000 chall && \
    groupadd -g 1001 solve && \
    useradd -rm -d /home/solve -s /bin/bash -g solve -u 1001 solve && \
    echo 'chall:chall' | chpasswd

COPY ./chall /home/chall/chall
COPY ./flag /home/chall/flag

RUN chmod -R 733 /tmp/ && \
    chown -R solve:solve /home/chall && \
    chmod -R 555 /home/chall && \
    chmod 500 /home/chall/flag && \
    chmod 4555 /home/chall/chall && \
    echo "set +o history" >> /home/chall/.bashrc 

RUN service ssh start

EXPOSE 22

CMD ["/usr/sbin/sshd","-D"]